package nl.hva.ict.ss;

import java.util.LinkedList;

public class AdvancedSorts {

    /**
     * Implement quicksort using LinkedList only! Usage of anything but LinkedList will result in failing the assignment!
     * @param unsorted
     * @param <E>
     * @return
     */
    public static <E extends Comparable<E>> LinkedList<E> quickSort(LinkedList<E> unsorted) {
//        E first = unsorted.getFirst();
        LinkedListQuicksort.quick(unsorted, 0, unsorted.size());
        return unsorted;
    }

    /**
     * Implement quicksort using arrays only! Usage of anything but arrays will result in failing the assignment!
     * @param unsorted
     * @param <E>
     * @return
     */
    public static <E extends Comparable<E>> E[] quickSort(E[] unsorted) {
        if (unsorted ==null || unsorted.length==0){
            return unsorted;
        }

        ArrayQuicksort.sort(unsorted, 0, unsorted.length);
        return unsorted;
    }

}
