package nl.hva.ict.ss;

public class ArrayQuicksort {
    public static <E extends Comparable<E>> void sort(E[] array, int start, int end){
        if (start + 1 >= end){
            return;
        }
        int mid = partition(array, start, end);
        sort(array, start, mid);
        sort(array, mid + 1, end);
    }

    private static <E extends Comparable<E>> int partition(E[] array, int start, int end){
        if (start + 1 >= end){
            return start;
        }

        E pivot = array[start];
        int i = start;
        for (int j = start + 1; j < end; ++j) {
            if (array[j].compareTo(pivot) < 0) {
                i += 1;
                swap(array, i, j);
            }
        }
        swap(array, start, i);
        return i;
    }

    private static <E extends Comparable<E>> void swap(E[] array, int left, int right){
        E tmp = array[left];
        array[left] = array[right];
        array[right] = tmp;
    }
}
