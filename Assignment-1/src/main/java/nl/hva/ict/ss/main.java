package nl.hva.ict.ss;

import nl.hva.ict.ss.util.NameReader;

import java.security.SecureRandom;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class main {
    static AdvancedSorts sorter = new AdvancedSorts();


    public static void main(String[] args) {
        System.out.println("Starting...");
//        Player[] players = generateArrayStudents(100);
//        sortArrayStudents(players);
        LinkedList players = generateLinkedListStudents(10000);
        sorter.quickSort(players);
        for (int i = 0; i < players.size(); i++) {
            System.out.println(players.get(i).toString());
        }
    }

    public static Player[] generateArrayStudents(int arraySize){
        Random randomizer = new SecureRandom();
        Player[] players = new Player[arraySize];
        String[] firstNames = new NameReader("/firstnames.txt").getNames();
        String[] lastNames = new NameReader("/lastnames.txt").getNames();
        for (int i = 0; i < arraySize; i++) {
            String firstName = firstNames[randomizer.nextInt(firstNames.length)];
            String lastName = lastNames[randomizer.nextInt(lastNames.length)];
            players[i] = new Player(firstName, lastName, randomizer.nextInt(1000));
        }
        return players;
    }

    public static LinkedList<Player> generateLinkedListStudents(int arraySize){
        Random randomizer = new SecureRandom();
        LinkedList<Player> players = new LinkedList<>();
        String[] firstNames = new NameReader("/firstnames.txt").getNames();
        String[] lastNames = new NameReader("/lastnames.txt").getNames();
        for (int i = 0; i < arraySize; i++) {
            String firstName = firstNames[randomizer.nextInt(firstNames.length)];
            String lastName = lastNames[randomizer.nextInt(lastNames.length)];
            players.add(new Player(firstName, lastName, randomizer.nextInt(1000)));
        }

        return players;
    }


    public static Player[] sortArrayStudents(Player[] players){
        sorter.quickSort(players);
        for (int j = 0; j < players.length; j++) {
            System.out.println(players[j].toString());
        }
        return players;
    }
}
