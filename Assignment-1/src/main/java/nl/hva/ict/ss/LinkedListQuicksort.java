package nl.hva.ict.ss;

import java.util.Collections;
import java.util.LinkedList;

public class LinkedListQuicksort {
    public static <E extends Comparable<E>> void quick(LinkedList<E> list, int start, int end){
        if (start + 1 >= end){
            return;
        }
        int mid = partition(list, start, end);
        quick(list, start, mid);
        quick(list, mid + 1, end);
    }

    private static <E extends Comparable<E>> int partition(LinkedList<E> list, int start, int end){
        if (start + 1 >= end){
            return start;
        }

        E pivot = list.get(start);
        int i = start;
        for (int j = start + 1; j < end; ++j) {
            if (list.get(j).compareTo(pivot) < 0) {
                i += 1;
                swap(list, i, j);
            }
        }
        swap(list, start, i);
        return i;
    }

    private static <E extends Comparable<E>> void swap(LinkedList<E> list, int left, int right){
        E tmp = list.get(left);
        list.set(left, list.get(right));
        list.set(right, tmp);
    }
}
