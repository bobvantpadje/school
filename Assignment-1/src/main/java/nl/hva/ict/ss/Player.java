package nl.hva.ict.ss;

/**
 * Player keeps track of the name and the highscore for a player.
 * Once created the name can't be changed. The best score for this player
 * can be changed during the lifetime of an instance.
 */
public class Player implements Comparable<Player> {
    private String firstName;
    private String lastName;
    private long highScore;

    public Player(String firstName, String lastName, long highScore) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.highScore = highScore;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public long getHighScore() {
        return highScore;
    }

    public void setHighScore(long highScore) {
        this.highScore = highScore;
    }

    @Override
    public String toString() {
        return lastName + ", "+ firstName + " "+ highScore;
    }

    @Override
    public int compareTo(Player other) {
        // First off we check the highscores
        if ( this.highScore != other.getHighScore()) {
            if (this.highScore > other.getHighScore()) {
                return 1;
            }
            return -1;
        // Here we check on lastname if the highscores are the same
        } else if(this.lastName.compareTo(other.getLastName()) != 0) {
            if (this.lastName.compareTo(other.getLastName()) > 0) {
                return -1;
            }
            return 1;
        // And last but not least when the lastname is also the same we check on firstname
        } else if (this.firstName.compareTo(other.getFirstName()) != 0) {
            if (this.firstName.compareTo(other.getFirstName()) > 0) {
                return -1;
            }
            return 1;
        }
        return 0;
    }
}
