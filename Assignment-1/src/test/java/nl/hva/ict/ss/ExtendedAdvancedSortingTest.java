package nl.hva.ict.ss;

import nl.hva.ict.ss.util.NameReader;
import org.junit.BeforeClass;
import org.junit.Test;

import java.security.SecureRandom;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import static junit.framework.TestCase.assertEquals;

/**
 * Put your tests that show you implemented the code correctly in this class.
 * Any test placed at a different class will be ignored!
 * Failing to add tests here will result in failing the assignment!
 */
public class ExtendedAdvancedSortingTest extends AdvancedSortingTest {
    @Test
    public final void shouldSortArrayOnHighScore() {
        Player[] players = new Player[5];
        players[0] = new Player("aaa", "aaa", 1);
        players[1] = new Player("aaa", "aaa", 4);
        players[2] = new Player("aaa", "aaa", 5);
        players[3] = new Player("aaa", "aaa", 3);
        players[4] = new Player("aaa", "aaa", 2);

        AdvancedSorts.quickSort(players);
        assertEquals(1, players[0].getHighScore());
        assertEquals(2, players[1].getHighScore());
        assertEquals(3, players[2].getHighScore());
        assertEquals(4, players[3].getHighScore());
        assertEquals(5, players[4].getHighScore());
    }

    @Test
    public final void shouldSortArrayOnLastName() {
        Player[] players = new Player[5];
        players[0] = new Player("bbb", "ddd", 1);
        players[1] = new Player("aaa", "eee", 1);
        players[2] = new Player("aaa", "bbb", 1);
        players[3] = new Player("ccc", "aaa", 1);
        players[4] = new Player("aaa", "ccc", 1);

        AdvancedSorts.quickSort(players);
        assertEquals("eee", players[0].getLastName());
        assertEquals("ddd", players[1].getLastName());
        assertEquals("ccc", players[2].getLastName());
        assertEquals("bbb", players[3].getLastName());
        assertEquals("aaa", players[4].getLastName());
    }

    @Test
    public final void shouldSortArrayOnFirstName() {
        Player[] players = new Player[5];
        players[0] = new Player("aaa", "aaa", 1);
        players[1] = new Player("bbb", "aaa", 1);
        players[2] = new Player("ccc", "aaa", 1);
        players[3] = new Player("ddd", "aaa", 1);
        players[4] = new Player("eee", "aaa", 1);

        AdvancedSorts.quickSort(players);
        assertEquals("eee", players[0].getFirstName());
        assertEquals("ddd", players[1].getFirstName());
        assertEquals("ccc", players[2].getFirstName());
        assertEquals("bbb", players[3].getFirstName());
        assertEquals("aaa", players[4].getFirstName());
    }

    @Test
    public final void shouldSortLinkedListOnHighScore() {
        LinkedList<Player> players = new LinkedList<>();
        players.add(new Player("aaa", "aaa", 3));
        players.add(new Player("aaa", "aaa", 5));
        players.add(new Player("aaa", "aaa", 1));
        players.add(new Player("aaa", "aaa", 2));
        players.add(new Player("aaa", "aaa", 4));

        AdvancedSorts.quickSort(players);
        assertEquals(1, players.get(0).getHighScore());
        assertEquals(2, players.get(1).getHighScore());
        assertEquals(3, players.get(2).getHighScore());
        assertEquals(4, players.get(3).getHighScore());
        assertEquals(5, players.get(4).getHighScore());
    }

    @Test
    public final void shouldSortLinkedListOnLastName() {
        LinkedList<Player> players = new LinkedList<>();
        players.add(new Player("aaa", "bbb", 1));
        players.add(new Player("aaa", "eee", 1));
        players.add(new Player("aaa", "ccc", 1));
        players.add(new Player("aaa", "ddd", 1));
        players.add(new Player("aaa", "aaa", 1));

        AdvancedSorts.quickSort(players);
        assertEquals("eee", players.get(0).getLastName());
        assertEquals("ddd", players.get(1).getLastName());
        assertEquals("ccc", players.get(2).getLastName());
        assertEquals("bbb", players.get(3).getLastName());
        assertEquals("aaa", players.get(4).getLastName());
    }

    @Test
    public final void shouldSortLinkedListFirstName() {
        LinkedList<Player> players = new LinkedList<>();
        players.add(new Player("bbb", "aaa", 1));
        players.add(new Player("ddd", "aaa", 1));
        players.add(new Player("aaa", "aaa", 1));
        players.add(new Player("ccc", "aaa", 1));
        players.add(new Player("eee", "aaa", 1));

        AdvancedSorts.quickSort(players);
        assertEquals("eee", players.get(0).getFirstName());
        assertEquals("ddd", players.get(1).getFirstName());
        assertEquals("ccc", players.get(2).getFirstName());
        assertEquals("bbb", players.get(3).getFirstName());
        assertEquals("aaa", players.get(4).getFirstName());
    }

}
