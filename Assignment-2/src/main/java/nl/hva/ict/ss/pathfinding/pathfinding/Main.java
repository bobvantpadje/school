package nl.hva.ict.ss.pathfinding.pathfinding;

import nl.hva.ict.ss.pathfinding.tileworld.TileWorld;
import nl.hva.ict.ss.pathfinding.tileworld.TileWorldUtil;
import nl.hva.ict.ss.pathfinding.weigthedgraph.AdjMatrixEdgeWeightedDigraph;
import nl.hva.ict.ss.pathfinding.weigthedgraph.DirectedEdge;
import nl.hva.ict.ss.pathfinding.weigthedgraph.EdgeWeightedDigraph;
import static java.util.concurrent.TimeUnit.NANOSECONDS;

/**
 * TODO make sure your code is compliant with the HBO-ICT coding conventions!
 * @author ???
 */
public class Main {


    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        FloydMarchall(21);
//        Dijkstra(22);

    }


    public static void FloydMarchall(int count) {
        for (int i = 1; i <= count; i++) {
            System.out.println("FloydMarchall");
            System.out.println("Tile #" + i);
            TileWorld t = new TileWorldUtil("i" + i + ".png");

            int startV = t.findStartIndex();
            int endV = t.findEndIndex();
            long start = System.nanoTime();

            EdgeWeightedDigraph e = new EdgeWeightedDigraph("i" + i);
            AdjMatrixEdgeWeightedDigraph a = e.createAdjMatrixEdgeWeightedDigraph();

            FloydWarshall fw = new FloydWarshall(a);

            System.out.println("Start: " + startV);
            System.out.println("End: " + endV);
            if (fw.hasPath(startV, endV)) {
                System.out.println("Distance: " + fw.dist(startV, endV));
                int size = 0;
                for (DirectedEdge de : fw.path(startV, endV)) {
                    size++;
                }
                System.out.println("Steps: " + size);
                e.tekenPad(fw.path(startV, endV));
                e.save("fw" + i);
                System.out.println("Knooppunten: " + a.V());
                System.out.println("knopen: " + fw.getCounter());
            } else {
                System.out.println("There is no path :'(");
            }
            System.out.println("---------------");

            long elapsed = System.nanoTime() - start;
            System.out.println("Elapsed: " + NANOSECONDS.toMicros(elapsed));

            t.show("i" + i, t.getHeight(), t.getWidth());
        }
    }

    public static void Dijkstra(int count) {
        for (int i = 1; i <= count; i++) {
            System.out.println("Tile #" + i);
            TileWorld t = new TileWorldUtil("i" + i + ".png");

            int startV = t.findStartIndex();
            int endV = t.findEndIndex();
            long start = System.nanoTime();
            EdgeWeightedDigraph e = new EdgeWeightedDigraph("i" + i);
            Dijkstra d = new Dijkstra(e, startV);
            System.out.println("Start: " + startV);
            System.out.println("End: " + endV);
            if (d.hasPathTo(endV)) {
                int length = 0;
                double kosten = 0;
                System.out.println("Aantal onderzochte knopen; " +
                        d.getCounter());
// //
                for (DirectedEdge de : d.pathTo(endV)) {
                    length++;
                    kosten += de.weight();
                }
                e.tekenPad(d.pathTo(endV));
                e.save("d" + i);
                e.show("d", "i");
                System.out.println("-----------------------------");
                System.out.println("Dijkstra");
                System.out.println("Steps: " + length);
                System.out.println("Knooppunten: " + e.V());
                System.out.println("Cost van pad: " + kosten);
                System.out.println("Lengte kortste pad: " +
                        d.distTo(e.getEnd()));
            } else {
                System.out.println("Steps: Geen pad gevonden!");
            }
        }
    }
}
    

